﻿using System;
using Telegram.Bot;
using Telegram.Bot.Extensions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using System.Data;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;
using System.Text.RegularExpressions;

namespace tgReminder
{
    class Program
    {
        
        static DB db = new DB();
        static List<Tuple<int, string, string, string, string, int>> items = new List<Tuple<int, string, string, string, string, int>>();
        static List<string> usses = new List<string>();

        public static void printNumber(object data)
        {
            TelegramBotClient botcl = (TelegramBotClient)data;

            var command = db.getConnection().CreateCommand();
            command.CommandText = ("SELECT * FROM Useres");

            db.openConnection();
            using (SqliteDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    usses.Add((string)reader.GetValue(2));
                }
            }

            db.closeConnection();
            foreach (string s in usses)
            {
                var command1 = db.getConnection().CreateCommand();
                command1.CommandText = ($"SELECT * FROM '{s}'");

                db.openConnection();
                using (SqliteDataReader reader = command1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var fir = reader.GetValue(0);
                        var tr = reader.GetValue(1);
                        var fo = reader.GetValue(2);
                        var mes = reader.GetValue(3);
                        var us = reader.GetValue(4);
                        var cha = reader.GetValue(5);
                        items.Add(new Tuple<int, string, string, string, string, int>((int)(long)fir, (string)tr, (string)fo, (string)mes, (string)us, (int)(long)cha));
                    }
                }
                db.closeConnection();
            }
            while (true)
            {
                Thread.Sleep(1000);
                foreach (Tuple<int, string, string, string, string, int> s in items)
                {
                    if (s.Item2 + ":00" == DateTime.Now.ToString().Substring(11) && s.Item3.Contains(DateTime.Now.DayOfWeek.ToString()))
                    {
                        Console.WriteLine("Res");
                        botcl.SendTextMessageAsync(s.Item6, s.Item4);
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            var botClient = new TelegramBotClient("2125884152:AAFXfwQtBhfK0i8FayW5fnnlYE_e5hym8Tc");

            Thread timeChecker = new Thread(new ParameterizedThreadStart(printNumber));
            timeChecker.IsBackground = true;
            timeChecker.Start(botClient);
            Thread.Sleep(5000);

            using var cts = new CancellationTokenSource();
            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = { }
            };
            botClient.StartReceiving(
                HandleUpdateAsync,
                HandleErrorAsync,
                receiverOptions,
                cancellationToken: cts.Token
                );

            var me = botClient.GetMeAsync();

            Console.WriteLine($"Start listening for @{me.Result.Username}");
            Console.ReadLine();
            
        }

        static string username2;

        static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            if (update.Type != UpdateType.Message)
                return;

            if (update.Message!.Type != MessageType.Text)
                return;

            var chatId = update.Message.Chat.Id;
            var username = "@" + update.Message.Chat.Username;
            Console.WriteLine(username);
            var messageText = update.Message.Text;

            texter userTexter = new texter();

            Console.WriteLine($"Received a '{messageText}' message in chat {chatId}.");

            if (messageText == "/start")
            {
                if (!tableExists(username))
                {

                    Message sentMessage = await botClient.SendTextMessageAsync(
                     chatId: chatId,
                     text: "Вы зарегистрированы. Чтобы поставить себе напоминалку введите:'text';время:**:**;дни:1,2,*,7",
                     cancellationToken: cancellationToken);

                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"INSERT INTO Useres (chatid, username, privelegy, dialog) VALUES ('{chatId}', '{username}', 'user', 'none')";

                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();

                    db.creatNapomList(username);
                }
                return;
            }


            if (!tableExists(username))
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "напишите /start",
                        cancellationToken: cancellationToken);
                return;
            }

            

            if (userTexter.checkJson(messageText) && tableExists(username))
            {
                Console.WriteLine("Checker");
                if (userTexter.msStatus == "user")
                {
                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"INSERT INTO '{username}' (data, den, word, username, chatId) VALUES ('{userTexter.TimeCheck}', '{userTexter.getDayOfWeek(userTexter.DniCheck)}', '{userTexter.wordCheck}', '{username}', '{chatId}')";
                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();

                    items.Add(new Tuple<int, string, string, string, string, int>(0, userTexter.TimeCheck, userTexter.getDayOfWeek(userTexter.DniCheck), userTexter.wordCheck, username, (int)chatId));
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "Вы добавили напоминание: \n" + userTexter.wordCheck + "\n" + userTexter.getDenForMessage(userTexter.DniCheck) + "\n" + userTexter.TimeCheck,
                        cancellationToken: cancellationToken);

                }
                else
                {
                    try
                    {
                        db.closeConnection();
                        if (getPriv(username) == "admin")
                        {
                            string spisok = "";
                            foreach (string us in userTexter.Users)
                            {
                                var command = db.getConnection().CreateCommand();
                                command.CommandText = $"INSERT INTO '{us}' (data, den, word, username, chatId) VALUES ('{userTexter.TimeCheck}', '{userTexter.getDayOfWeek(userTexter.DniCheck)}', '{userTexter.wordCheck}', '{us}', '{chatId}')";
                                db.openConnection();
                                command.ExecuteNonQuery();
                                db.closeConnection();
                                items.Add(new Tuple<int, string, string, string, string, int>(0, userTexter.TimeCheck, userTexter.getDayOfWeek(userTexter.DniCheck), userTexter.wordCheck, us, (int)chatId));

                                spisok += ", " + us;
                                Message sentMessagetouser = await botClient.SendTextMessageAsync(
                                    chatId: userTexter.findChatIdByUsername(us),
                                    text: "Вам добавили напоминание: \n" + userTexter.wordCheck + "\n" + userTexter.getDenForMessage(userTexter.DniCheck) + "\n" + userTexter.TimeCheck,
                                    cancellationToken: cancellationToken);
                            }
                            spisok = spisok.Substring(2);
                            Message sentMessage = await botClient.SendTextMessageAsync(
                                chatId: chatId,
                                text: "Вы добавили напоминание: \n" + userTexter.wordCheck + "\n" + userTexter.getDenForMessage(userTexter.DniCheck) + "\n" + userTexter.TimeCheck + " для " + spisok,
                                cancellationToken: cancellationToken);
                        }
                        else
                        {
                            Message sentMessage = await botClient.SendTextMessageAsync(
                                    chatId: chatId,
                                    text: "Вы не администратор\n",
                                    cancellationToken: cancellationToken);
                        }
                    }
                    catch
                    {
                        Message sentMessage = await botClient.SendTextMessageAsync(
                                    chatId: chatId,
                                    text: "ошибка",
                                    cancellationToken: cancellationToken);
                    }
                }

            }

            if (messageText == "/checkstatus")
            {
                Message sentMessage2 = await botClient.SendTextMessageAsync(
                   chatId: chatId,
                   text: "ваш статус: " + getPriv(username),
                   cancellationToken: cancellationToken);
            }

            if (messageText == "/userlist")
            {
                Message sentMessage2 = await botClient.SendTextMessageAsync(
                   chatId: chatId,
                   text: username,
                   cancellationToken: cancellationToken);
                var command1 = db.getConnection().CreateCommand();
                command1.CommandText = ($"SELECT * FROM Useres");

                db.openConnection();
                using (SqliteDataReader reader = command1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var us = reader.GetValue(2);
                        Message sentMessage = await botClient.SendTextMessageAsync(
                   chatId: chatId,
                   text: (string)us,
                   cancellationToken: cancellationToken);
                    }
                }

                db.closeConnection();
            }

            if (messageText == "/setreminder")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                     chatId: chatId,
                     text: "'text';время:**:**;дни:1,2,*,7",
                     cancellationToken: cancellationToken);

            }

            if (messageText == "/myremlist")
            {
                var command1 = db.getConnection().CreateCommand();
                command1.CommandText = ($"SELECT * FROM '{username}'");

                string sio = "";

                db.openConnection();
                using (SqliteDataReader reader = command1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var fir = reader.GetValue(0);
                        var tr = reader.GetValue(1);
                        var fo = reader.GetValue(2);
                        var mes = reader.GetValue(3);
                        var us = reader.GetValue(4);
                        var cha = reader.GetValue(5);

                        sio += tr + "\t-\t" + fo + "\t-\t" + mes + "\n";
                    }
                }

                db.closeConnection();


                if (sio == "")
                {
                    Message sentMessage1 = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "у вас нету напоминаний",
                    cancellationToken: cancellationToken);
                }
                else
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: sio,
                        cancellationToken: cancellationToken);
                }
            }

            if (messageText == "/giveadmin" && getPriv(username) == "creator")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                     chatId: chatId,
                     text: "впишите username пользователя",
                     cancellationToken: cancellationToken);
                setDialog(username, "getadmin");
                return;
            }
            if (getDialog(username) == "getadmin")
            {
                try
                {
                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"UPDATE Useres SET privelegy = 'admin' WHERE username = '{messageText}'";
                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();
                }
                catch
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                     chatId: chatId,
                     text: "ошибка",
                     cancellationToken: cancellationToken);
                }
            }


            if (messageText == "/remlist" && getPriv(username) == "admin")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "впишите username пользователя",
                    cancellationToken: cancellationToken);
                setDialog(username, "getspisokreminder");
                return;
            }
            if (getDialog(username) == "getspisokreminder")
            {
                try
                {
                    var command1 = db.getConnection().CreateCommand();
                    command1.CommandText = ($"SELECT * FROM '{messageText}'");

                    string sio = "";

                    db.openConnection();
                    using (SqliteDataReader reader = command1.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var fir = reader.GetValue(0);
                            var tr = reader.GetValue(1);
                            var fo = reader.GetValue(2);
                            var mes = reader.GetValue(3);
                            var us = reader.GetValue(4);
                            var cha = reader.GetValue(5);

                            sio += tr + "\t-\t" + fo + "\t-\t" + mes + "\n";
                        }
                    }

                    db.closeConnection();


                    if (sio == "")
                    {
                        Message sentMessage1 = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "у него нету напоминаний",
                        cancellationToken: cancellationToken);
                    }
                    else
                    {
                        Message sentMessage = await botClient.SendTextMessageAsync(
                            chatId: chatId,
                            text: sio,
                            cancellationToken: cancellationToken);
                    }
                    setDialog(username, "none");
                }
                catch
                {
                    Message sentMessage1 = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "ошибка!",
                        cancellationToken: cancellationToken);
                    setDialog(username, "none");
                }
            }


            if (messageText == "/deleteall")
            {
                for (int j = 0; j < 10; j++)
                {
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (items[i].Item5 == username)
                        {

                            items.RemoveAt(i);
                        }
                    }
                }

                var command = db.getConnection().CreateCommand();
                command.CommandText = $"DELETE FROM '{username}' WHERE chatid = '{chatId}'";
                db.openConnection();
                command.ExecuteNonQuery();
                db.closeConnection();

                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "Ваши напоминания удалились",
                    cancellationToken: cancellationToken);

            }

            if (messageText == "/delete")
            {
                var command1 = db.getConnection().CreateCommand();
                command1.CommandText = ($"SELECT * FROM '{username}'");
                string sio = " ";
                db.openConnection();
                using (SqliteDataReader reader = command1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var fir = reader.GetValue(0);
                        var tr = reader.GetValue(1);
                        var fo = reader.GetValue(2);
                        var mes = reader.GetValue(3);
                        var us = reader.GetValue(4);
                        var cha = reader.GetValue(5);

                        sio += fir + "\t-\t" + tr + "\t-\t" + fo + "\t-\t" + mes + "\n";
                    }
                }
                db.closeConnection();

                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: $"впишите номер напоминания\n{sio}",
                    cancellationToken: cancellationToken);

                setDialog(username, "deletespisok");
                return;
            }

            if (getDialog(username) == "deletespisok")
            {
                try
                {
                    int del = 0;
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (items[i].Item1 == Convert.ToInt32(messageText) && items[i].Item5 == username)
                        {
                            del = i;
                        }
                    }

                    items.RemoveAt(del);

                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"DELETE FROM '{username}' WHERE _id = '{messageText}'";

                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();

                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "Ваше напоминание удалилось",
                        cancellationToken: cancellationToken);
                    setDialog(username, "none");
                }
                catch
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "Ошибка",
                        cancellationToken: cancellationToken);
                    setDialog(username, "none");
                }
            }


            if (messageText == "/deleteu" && getPriv(username) == "admin")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: $"впишите никнейм юзера",
                    cancellationToken: cancellationToken);


                setDialog(username, "deletespisoku");
                return;
            }

            if (getDialog(username) == "deletespisoku")
            {
                try
                {
                    username2 = messageText;
                    var command1 = db.getConnection().CreateCommand();
                    command1.CommandText = ($"SELECT * FROM '{messageText}'");
                    string sio = "";
                    db.openConnection();
                    using (SqliteDataReader reader = command1.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var fir = reader.GetValue(0);
                            var tr = reader.GetValue(1);
                            var fo = reader.GetValue(2);
                            var mes = reader.GetValue(3);
                            var us = reader.GetValue(4);
                            var cha = reader.GetValue(5);

                            sio += fir + "\t-\t" + tr + "\t-\t" + fo + "\t-\t" + mes + "\n";
                        }
                    }


                    db.closeConnection();

                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: $"впишите номер напоминания\n{sio}",
                        cancellationToken: cancellationToken);


                    setDialog(username, "deletespisoku2");
                    return;
                }
                catch
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: $"ошибка",
                        cancellationToken: cancellationToken);


                    setDialog(username, "none");
                }
            }

            if (getDialog(username) == "deletespisoku2")
            {
                try
                {
                    int del = 0;
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (items[i].Item1 == Convert.ToInt32(messageText) && items[i].Item5 == username2)
                        {
                            del = i;
                        }
                    }

                    items.RemoveAt(del);
                    Console.WriteLine(username2);
                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"DELETE FROM '{username2}' WHERE _id = '{messageText}'";

                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();

                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "это напоминание удалилось",
                        cancellationToken: cancellationToken);
                    setDialog(username, "none");
                }
                catch
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "ошибка!" + username2,
                        cancellationToken: cancellationToken);
                    setDialog(username, "none");
                }
            }

            if (messageText == "/deleteallu" && getPriv(username) == "admin")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "впишите username пользователя",
                    cancellationToken: cancellationToken);
                setDialog(username, "deleteallspisok");
            }

            if (getDialog(username) == "deleteallspisok")
            {
                try
                {
                    for (int j = 0; j < 10; j++)
                    {
                        for (int i = 0; i < items.Count; i++)
                        {
                            if (items[i].Item5 == messageText)
                            {

                                items.RemoveAt(i);
                            }
                        }
                    }

                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"DELETE FROM {messageText} WHERE username = '{messageText}'";
                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();

                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "Его напоминания удалились",
                        cancellationToken: cancellationToken);
                    setDialog(username, "none");
                }
                catch
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "ошибка!",
                        cancellationToken: cancellationToken);
                    setDialog(username, "none");
                }
            }
        }

        public static void setDialog(string username, string dialogSt)
        {
            var command = db.getConnection().CreateCommand();
            command.CommandText = $"UPDATE Useres SET dialog = '{dialogSt}' WHERE username = '{username}'";
            db.openConnection();
            command.ExecuteNonQuery();
            db.closeConnection();
        }

        public static string getDialog(string username)
        {
            var command2 = db.getConnection().CreateCommand();
            command2.CommandText = $"SELECT dialog FROM Useres WHERE username = '{username}'";
            db.openConnection();
            string stat = "user";
            using (SqliteDataReader reader = command2.ExecuteReader())
            {
                while (reader.Read())
                {
                    stat = (string)reader.GetValue(0);
                }
            }

            db.closeConnection();
            Console.WriteLine(stat);
            return stat;
        }

        public static string getPriv(string username)
        {
            var command2 = db.getConnection().CreateCommand();
            command2.CommandText = $"SELECT privelegy FROM Useres WHERE username = '{username}'";
            db.openConnection();
            string stat = "user";
            using (SqliteDataReader reader = command2.ExecuteReader())
            {
                while (reader.Read())
                {
                    stat = (string)reader.GetValue(0);
                }
            }

            db.closeConnection();
            Console.WriteLine(stat);
            return stat;
        }

        public static bool tableExists(string usforCheck)
        {
            string sqlExpression = $"SELECT * FROM Useres";
            string imena = "";
            using (db.getConnection())
            {
                db.openConnection();

                SqliteCommand command = new SqliteCommand(sqlExpression, db.getConnection());
                using (SqliteDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows) // если есть данные
                    {
                        while (reader.Read())   // построчно считываем данные
                        {
                            imena += reader.GetValue(2);
                        }

                    }
                }
                db.closeConnection();

            }
            if (imena.Contains(usforCheck))
            {
                return true;
            }
            return false;

        }

        class texter
        {
            public string wordCheck { get; set; }
            public string VremyaCheck { get; set; }
            public string TimeCheck { get; set; }
            public string DayCheck { get; set; }
            public string DniCheck { get; set; }
            public string PolzCheck { get; set; }
            public string UserCheck { get; set; }
            public string[] Users { get; set; }
            public string msStatus { get; set; }

            public bool checkJson(string message)
            {
                try
                {

                    int fiZa = (message.IndexOf(";")) + 1;
                    int leWord = fiZa - 1;
                    wordCheck = (message.Substring(0, leWord));

                    int fiDv = (message.IndexOf(":"));
                    int length = fiDv - fiZa;
                    VremyaCheck = (message.Substring(fiZa, length));

                    int seZa = (message.IndexOf(";", fiDv));
                    int leTime = seZa - fiDv;
                    TimeCheck = message.Substring(fiDv + 1, leTime - 1);

                    int seDv = (message.IndexOf(":", seZa));
                    int leDay = seDv - seZa + 1;
                    DayCheck = message.Substring(seZa + 1, leDay - 2);

                    int thZa = (message.IndexOf(";", seDv));
                    int leDni = thZa - 1 - seDv + 1;
                    DniCheck = message.Substring(seDv + 1, leDni - 1);

                    int thDv = (message.IndexOf(":", thZa));
                    int lePolz = thDv - thZa + 1;
                    PolzCheck = message.Substring(thZa + 1, lePolz - 2);

                    int leUser = message.Length - 1 - thDv + 1;
                    UserCheck = message.Substring(thDv + 1, leUser - 1);

                    VremyaCheck = VremyaCheck.Replace(" ", ""); TimeCheck = TimeCheck.Replace(" ", "");
                    DayCheck = DayCheck.Replace(" ", "");
                    DniCheck = DniCheck.Replace(" ", "");
                    PolzCheck = PolzCheck.Replace(" ", "");
                    UserCheck = UserCheck.Replace(" ", "");

                    Console.WriteLine($"{wordCheck} {VremyaCheck} {TimeCheck} {DayCheck} {DniCheck} {PolzCheck} {UserCheck}");
                    if (VremyaCheck.ToLower() != "время")
                        return false;

                    if (DayCheck.ToLower() != "дни")
                        return false;

                    if (PolzCheck.ToLower() != "пользователи")
                        return false;

                    Users = UserCheck.Split(',');
                    msStatus = "admin";

                    string pattern = "H:mm";
                    DateTime parsedDate;
                    if (DateTime.TryParseExact(TimeCheck, pattern, null,
                                            DateTimeStyles.None, out parsedDate))
                    {
                        Console.WriteLine("Converted '{0}' to {1:H:mm}.",
                                            message, parsedDate);
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Unable to convert '{0}' to a date and time.",
                                            message);
                        return false;
                    }
                }
                catch
                {
                    try
                    {

                        int fiZa = (message.IndexOf(";")) + 1;
                        int leWord = fiZa - 1;
                        wordCheck = (message.Substring(0, leWord));

                        int fiDv = (message.IndexOf(":"));
                        int length = fiDv - fiZa;
                        VremyaCheck = (message.Substring(fiZa, length));

                        int seZa = (message.IndexOf(";", fiDv));
                        int leTime = seZa - fiDv;
                        TimeCheck = message.Substring(fiDv + 1, leTime - 1);

                        int seDv = (message.IndexOf(":", seZa));
                        int leDay = seDv - seZa + 1;
                        DayCheck = message.Substring(seZa + 1, leDay - 2);

                        int leDni = message.Length - 1 - seDv + 1;
                        DniCheck = message.Substring(seDv + 1, leDni - 1);
                        VremyaCheck = VremyaCheck.Replace(" ", ""); TimeCheck = TimeCheck.Replace(" ", "");
                        DayCheck = DayCheck.Replace(" ", "");
                        DniCheck = DniCheck.Replace(" ", "");

                        if (VremyaCheck.ToLower() != "время")
                            return false;

                        if (DayCheck.ToLower() != "дни")
                            return false;
                        Console.WriteLine($"{wordCheck} {VremyaCheck} {TimeCheck} {DayCheck} {DniCheck}");
                        msStatus = "user";
                        string pattern = "H:mm";
                        DateTime parsedDate;
                        if (DateTime.TryParseExact(TimeCheck, pattern, null,
                                                DateTimeStyles.None, out parsedDate))
                        {
                            Console.WriteLine("Converted '{0}' to {1:H:mm}.",
                                                message, parsedDate);
                            return true;
                        }
                        else
                        {
                            Console.WriteLine("Unable to convert '{0}' to a date and time.",
                                                message);
                            return false;
                        }
                    }
                    catch
                    {
                        return false;
                    }
                }

            }

            public int findChatIdByUsername(string username)
            {
                int result = 0;
                var command = db.getConnection().CreateCommand();
                command.CommandText = ("SELECT * FROM Useres");
                db.openConnection();
                using (SqliteDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        var cha = reader.GetValue(1);
                        var us = reader.GetValue(2);
                        if ((string)us == username)
                            result = (int)(long)cha;
                    }
                }
                db.closeConnection();
                return result;
            }


            public string getDayOfWeek(string message)
            {
                string dni = "";
                if (message.Contains('1'))
                {
                    dni = dni + ", Monday";
                }
                if (message.Contains('2'))
                {
                    dni = dni + ", Tuesday";
                }
                if (message.Contains('3'))
                {
                    dni = dni + ", Wednesday";
                }
                if (message.Contains('4'))
                {
                    dni = dni + ", Thursday";
                }
                if (message.Contains('5'))
                {
                    dni = dni + ", Friday";
                }
                if (message.Contains('6'))
                {
                    dni = dni + ", Saturday";
                }
                if (message.Contains('7'))
                {
                    dni = dni + ", Sunday";
                }
                return dni.Substring(1);

            }

            public string getDenForMessage(string message)
            {
                string dni = "";
                if (message.Contains('1'))
                {
                    dni = dni + ", понедельник";
                }
                if (message.Contains('2'))
                {
                    dni = dni + ", вторник";
                }
                if (message.Contains('3'))
                {
                    dni = dni + ", среда";
                }
                if (message.Contains('4'))
                {
                    dni = dni + ", четверг";
                }
                if (message.Contains('5'))
                {
                    dni = dni + ", пятница";
                }
                if (message.Contains('6'))
                {
                    dni = dni + ", суббота";
                }
                if (message.Contains('7'))
                {
                    dni = dni + ", воскрсенье";
                }
                return dni.Substring(1);
            }
        }

        static public bool checkOnDate(string message)
        {
            if (message.Length < 6)
            {
                return false;
            }
            message = message.Replace(" ", "");
            if (message[5] != '-')
            {
                return false;
            }

            string time = message.Substring(0, 5);
            Console.WriteLine(time);
            string pattern = "H:mm";
            DateTime parsedDate;
            if (DateTime.TryParseExact(time, pattern, null,
                                    DateTimeStyles.None, out parsedDate))
            {
                Console.WriteLine("Converted '{0}' to {1:H:mm}.",
                                    message, parsedDate);
                return true;
            }
            else
            {
                Console.WriteLine("Unable to convert '{0}' to a date and time.",
                                    message);
                return false;
            }

        }

        static Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                Telegram.Bot.Exceptions.ApiRequestException apiRequestException
                    => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }
    }
}
