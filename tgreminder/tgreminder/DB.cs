﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace tgReminder
{
    class DB
    {
        SqliteConnection connection = new SqliteConnection("Data Source=reminderd.db");

        public void openConnection()
        {
            if (connection.State == System.Data.ConnectionState.Closed)
                connection.Open();
        }
        public void creatDB()
        {
            connection.Open();
            SqliteCommand command = new SqliteCommand();
            command.Connection = connection;
            command.CommandText = "CREATE TABLE Useres(_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, chatid INTEGER NOT NULL, username TEXT NOT NULL, privelegy TEXT NOT NULL, dialog TEXT)";
            command.ExecuteNonQuery();
            connection.Close();
        }

        public void creatNapomList(string username)
        {
            connection.Open();
            SqliteCommand command = new SqliteCommand();
            command.Connection = connection;
            command.CommandText = $"CREATE TABLE '{username}' (_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, data TEXT NOT NULL, den TEXT NOT NULL, word TEXT NOT NULL, username TEXT NOT NULL, chatId INTEGER)";
            command.ExecuteNonQuery();
            connection.Close();
        }

        public void closeConnection()
        {
            if (connection.State == System.Data.ConnectionState.Open)
                connection.Close();
        }

        public SqliteConnection getConnection()
        {
            return connection;
        }
    }
}
